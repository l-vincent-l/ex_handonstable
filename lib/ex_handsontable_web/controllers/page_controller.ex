defmodule ExHandsontableWeb.PageController do
  use ExHandsontableWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def table(conn, _params) do
    render conn, "table.html"
  end

  def create_table(conn, %{"table_slug" => table_slug}) do
    redirect conn, to: page_path(conn, :table, table_slug)
  end
end
