defmodule ExHandsontableWeb.PageCommander do
  use Drab.Commander

  onload :page_loaded

  def page_loaded(socket) do
    {:ok, page} = exec_js socket, "window.location.pathname"
    max_row = ConCache.get_or_store(:data, {page, :max_row}, fn () -> 1 end)
    data = Enum.map(0..max_row, &(get_columns(page, &1)))
    data_json = Poison.encode!(data)

    exec_js socket, "window.init(#{data_json})"
  end

  defp get_columns(page, row) do
    max_column = ConCache.get_or_store(:data, {page, :max_column}, fn () -> 1 end)
    Enum.map(0..max_column, &(get_cell(page, row, &1)))
  end

  defp get_cell(page, row, column), do: ConCache.get(:data, {page, row, column})

  defhandler afterChange(socket, payload) do
    payload["changes"]
    |> Enum.each(&(setDataCell(socket, payload["page"], &1)))
  end

  defp setDataCell(socket, page, change) do
    [row, column, _old_value,  value] = change
    value = Phoenix.HTML.escape_javascript(value)
    max_row = max(ConCache.get(:data, {page, :max_row}), row)
    max_column = max(ConCache.get(:data, {page, :max_column}), column)

    ConCache.put(:data, {page, :max_row}, max_row)
    ConCache.put(:data, {page, :max_column}, max_column)
    ConCache.put(:data, {page, row, column}, value)

    js = "window.hot.setDataAtCell(#{row}, #{column}, '#{value}', 'drab')"
    broadcast_js socket, js
  end
end
