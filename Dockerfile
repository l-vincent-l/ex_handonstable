FROM elixir:1.6-alpine

RUN apk add nodejs-npm curl yarn

RUN mkdir phoenixapp

WORKDIR phoenixapp

COPY ./mix.exs /phoenixapp/mix.exs
COPY ./mix.lock /phoenixapp/mix.lock

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix do deps.get --only prod

COPY ./ /phoenixapp

ENV PORT 8080
ENV MIX_ENV prod

RUN mix do compile

RUN cd assets && yarn install && npm run deploy
RUN mix phx.digest

EXPOSE 8080

ENTRYPOINT ["mix", "phx.server"]
